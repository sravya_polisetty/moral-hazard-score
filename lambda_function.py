import ast
import psycopg2
import os
import numpy as np
import sklearn.preprocessing as sk
import postgres_functions as pgf
import boto3
from importlib import import_module
import pickle
from sklearn.externals import joblib
import lambdawarmer
import math as m

s3 = boto3.resource('s3')

# PostGres parameters
HOSTNAME = os.environ['PG_HOSTNAME']
USERNAME = os.environ['PG_USERNAME']
PASSWORD = os.environ['PG_PASSWORD']
DATABASE = os.environ['PG_DATABASE']
BUCKET = os.environ['PG_BUCKET']



# Initialize PostGres connections
conn = psycopg2.connect(host=HOSTNAME, user=USERNAME, password=PASSWORD, dbname=DATABASE)

def read_joblib(pkl_obj):
    with open('/tmp/' + pkl_obj,'wb') as data:
        s3.Bucket(BUCKET).download_fileobj(pkl_obj,data)
    with open('/tmp/' + pkl_obj, 'rb') as data:
        scaler_obj = joblib.load(data)
        
    return scaler_obj
    
@lambdawarmer.warmer(send_metric=False)
def lambda_handler(event, context):

    oak = event['oak']
    # Pull data from Postgres
    try:
        data = pgf.Find_OAK_Details(oak)
        
        if(len(data) == 0):
            raise ValueError('Invalid OAK')
            
        elif(data[0]["avm_value"] is None or data[0]["va_replacementcost"] is None or data[0]["ag_yearbuilt"] is None):
            raise Exception('No feature for OAK')

        else:
            
            # Calculate Replacement Cost/AVM
            rc_avm = (data[0]['va_replacementcost'])/(data[0]['avm_value'])
    
            # Read converter and scaler objects from S3 bucket
            converter_rc_avm = read_joblib('Converter_RC_AVM.pkl')
            scaler_rc_avm = read_joblib('Scaler_RC_AVM.pkl')
            converter_yr_built = read_joblib('Converter_Year.pkl')
            scaler_yr_built = read_joblib('Scaler_Year.pkl')
            converter_bni_score = read_joblib('Converter_bni_credit.pkl')
            scaler_bni_score = read_joblib('Scaler_bni_credit.pkl')
            converter_ers_score = read_joblib('Converter_ers_credit.pkl')
            scaler_ers_score = read_joblib('Scaler_ers_credit.pkl')
    
    
            # Transform the data related to the OAK using the objects
            new_rc_avm = converter_rc_avm.transform(np.array(rc_avm).reshape(1, 1))
            score_rc_avm = scaler_rc_avm.transform(new_rc_avm)
    
            new_yr_built = converter_yr_built.transform(np.array(data[0]['ag_yearbuilt']).reshape(1,1))
            score_yr_built =  11 - scaler_yr_built.transform(new_yr_built)
    
            new_bni_score = converter_bni_score.transform(np.array(data[0]['bni2_score_avg']).reshape(1,1))
            score_bni =  11 - scaler_bni_score.transform(new_bni_score)
    
            new_ers_score = converter_ers_score.transform(np.array(data[0]['ers_score_avg']).reshape(1,1))
            score_ers =  11 - scaler_ers_score.transform(new_ers_score)
    
    
            moral_threat_score = round(((0.6*score_rc_avm)+(0.2*score_yr_built)+(0.1*score_bni) + (0.1*score_ers))[0][0],2)
            

             
            return {
                'statusCode': 200,
                'score': moral_threat_score
            }
            
    except(Exception) as e:
        message = { 'type' : 'Bad Request','statusCode': 400, 'message': e.args}
        return(message)