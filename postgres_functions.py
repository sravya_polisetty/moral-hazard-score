import json
import psycopg2
import os
import numpy as np
from psycopg2.extras import RealDictCursor
import postgres_functions as pgf


# PostGres parameters
HOSTNAME = os.environ['PG_HOSTNAME']
USERNAME = os.environ['PG_USERNAME']
PASSWORD = os.environ['PG_PASSWORD']
DATABASE = os.environ['PG_DATABASE']

conn = psycopg2.connect(host=HOSTNAME, user=USERNAME, password=PASSWORD, dbname=DATABASE)
# Execute PostGres query
def pg_query(connection, query):
    try:
        cur = connection.cursor(cursor_factory=RealDictCursor)
        cur.execute(query)
        res = cur.fetchall()
        cur.close()
        connection.commit()
        return res
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        message = {
            'type' : 'Bad Request',
            'httpStatus': 400,
            'message' : 'Cannot connect to PostGres'
            }
        raise Exception(message)
        

# Pull Data for oak
def Find_OAK_Details(oak):

    command = ("select a.avm_value, c.va_replacementcost, d.ag_yearbuilt,e.bni2_score_avg,e.ers_score_avg "
+ "from avm_v2.scores a inner join opta_data.addresses b on a.oak2 = b.oak2 "
+ "inner join opta_data.valuations c on b.oak2 = c.oak2 "
+ "inner join opta_data.construction_features d on c.oak2 = d.oak2 "
+ "inner join postalcode_level.credit_scores e on b.postal_code = e.postal_code "
+ "where a.score_year_month = (select max (score_year_month) from avm_v2.scores) and e.acquired_year = (select max(acquired_year) from postalcode_level.credit_scores) and a.oak2 = '{}';".format(oak))

    data = pg_query(conn,command)
    return data
    
   